const express = require("express");
const app = express();
const { success, getUniqueId } = require("./helpers");
const port = 3000;
const fs = require("fs");
const bodyparser = require("body-parser");
const pokeRoute = require("./routes/routes");
const sequilize = require("./config/sequilize");

app.use(bodyparser.json());

app.use("/api/pokemon", pokeRoute);

sequilize.initDb();

app.listen(port, () =>
  console.log(`Votre application est démarrée sur : http://localhost:${port}`)
);
