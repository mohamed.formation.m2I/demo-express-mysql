const { Pokemon } = require("../config/sequilize");

exports.createPokemon = (req, res) => {
  Pokemon.create(req.body).then((pokemon) => {
    const message = `Le pokémon ${req.body.name} a bien été crée.`;
    res.json({ message, data: pokemon });
  });
};

exports.deletepokemon = (req, res) => {
  Pokemon.findByPk(req.params.id).then((pokemon) => {
    const pokemonDeleted = pokemon;
    Pokemon.destroy({
      where: { id: pokemon.id },
    }).then((_) => {
      const message = `Le pokémon avec l'identifiant n°${pokemonDeleted.id} a bien été supprimé.`;
      res.json({ message, data: pokemonDeleted });
    });
  });
};

exports.findAllPokemons = (req, res) => {
  Pokemon.findAll().then((pokemons) => {
    const message = "La liste des pokémons a bien été récupérée.";
    res.json({ message, data: pokemons });
  });
};
exports.findPokemonByPk = (req, res) => {
  Pokemon.findByPk(req.params.id).then((pokemon) => {
    const message = "Un pokémon a bien été trouvé.";
    res.json({ message, data: pokemon });
  });
};

exports.putPokemon = (req, res) => {
  const id = req.params.id;
  Pokemon.update(req.body, {
    where: { id: id },
  }).then((_) => {
    Pokemon.findByPk(id).then((pokemon) => {
      const message = `Le pokémon ${pokemon.name} a bien été modifié.`;
      res.json({ message, data: pokemon });
    });
  });
};
