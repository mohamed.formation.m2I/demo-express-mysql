const express = require("express");
const router = express.Router();

const {
  createPokemon,
  deletepokemon,
  findAllPokemons,
  findPokemonByPk,
  putPokemon,
} = require("../controllers/controllerPokemon");

router.post("/", createPokemon);
router.delete("/:id", deletepokemon);
router.get("/", findAllPokemons);
router.get("/:id", findPokemonByPk);
router.put("/:id", putPokemon);

module.exports = router;
